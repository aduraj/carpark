from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^dashboard/', 'park_management.views.dashboard', name='park_management_dashboard'),
    url(r'^park_settings/', 'park_management.views.park_settings', name='park_management_park_settings'),
    url(r'^reservations/', 'park_management.views.reservations', name='park_management_reservations'),
    url(r'^edit_profile/', 'park_management.views.edit_profile', name='park_management_edit_profile'),
)
