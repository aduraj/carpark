from django import forms
from car_parks.models import CarPark, CarParkManager
from django.contrib.auth.models import User


class CarParkForm(forms.ModelForm):
    class Meta:
        model = CarPark
        fields = ('name', 'address', 'size', 'validity_minutes', 'open_at', 'close_at', 'time_granulation',
            'not_sooner_than')


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('username', 'email',)


class CarParkManagerForm(forms.ModelForm):
    class Meta:
        model = CarParkManager
        fields = ('firstname', 'lastname',)
