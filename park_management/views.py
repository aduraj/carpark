from django.db import transaction
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from forms import CarParkForm, CarParkManagerForm, UserForm


@login_required
def dashboard(request):
    return render(request, 'park_management/dashboard.html', build_context(request))


@login_required
def park_settings(request):
    current_park = get_current_park(request)
    if request.POST:
        form = CarParkForm(request.POST, instance=current_park)
        if form.is_valid():
            form.save()
            return redirect('park_management_dashboard')
    else:
        form = CarParkForm(instance=current_park)

    return render(request, 'park_management/park_settings.html', build_context(request, {
        'form': form
    }))


@login_required
def reservations(request):
    current_park = get_current_park(request)
    return render(request, 'park_management/reservations.html', build_context(request, {
        'reservations': current_park.reservations.filter(approved=True).all()
    }))


@login_required
def edit_profile(request):
    current_park_manager = get_current_park_manager(request)
    if request.POST:
        user_form = UserForm(request.POST, instance=request.user)
        car_park_manager_form = CarParkManagerForm(request.POST, instance=current_park_manager)

        if user_form.is_valid() and car_park_manager_form.is_valid():
            with transaction.atomic():
                user_form.save()
                car_park_manager_form.save()
            return redirect('park_management_dashboard')
    else:
        user_form = UserForm(instance=request.user)
        car_park_manager_form = CarParkManagerForm(instance=current_park_manager)

    return render(request, 'park_management/edit_profile.html', build_context(request, {
        'user_form': user_form,
        'car_park_manager_form': car_park_manager_form
    }))


def build_context(request, context={}):
    defaults = {
        'current_park_manager': get_current_park_manager(request),
        'current_park': get_current_park(request)
    }
    return dict(defaults.items() + context.items())


def get_current_park_manager(request):
    return request.user.car_park_manager


def get_current_park(request):
    return get_current_park_manager(request).car_park
