from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'carpark.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'car_parks.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^users/', RedirectView.as_view(pattern_name='park_management_park_settings')),
    url(r'^accounts/profile/', RedirectView.as_view(pattern_name='park_management_dashboard')),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^park_management/', include('park_management.urls')),
    url(r'^car_parks/', include('car_parks.urls')),
)
