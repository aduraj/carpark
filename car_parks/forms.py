from django import forms
from kitabu.search.available import Subjects
from car_parks.models import CarPark, CarParkReservation
from datetimewidget.widgets import DateTimeWidget
from datetime import datetime, timedelta
from kitabu.exceptions import SizeExceeded, ForbiddenHours, TimeUnitNotDivisible, TooSoon


def initial_start():
    return datetime.now() + timedelta(hours=1)

def initial_end():
    return initial_start() + timedelta(hours=1)


class CarParkSearchForm(forms.Form):
    address = forms.CharField(max_length=255, required=False)
    start = forms.DateTimeField(widget=DateTimeWidget, initial=initial_start)
    end = forms.DateTimeField(widget=DateTimeWidget, initial=initial_end)
    required_size = forms.IntegerField(min_value=1, initial=1)

    def search(self):
        if not self.is_valid():
            raise Exception('Form is not valid!')

        subject_manager = None
        
        if self.cleaned_data['address']:
            subject_manager = CarPark.objects.filter(address__icontains=self.cleaned_data['address'])
            if not subject_manager:
                return []

        searcher = Subjects(subject_model=CarPark, subject_manager=subject_manager)

        return searcher.valid_search(start=self.cleaned_data['start'], end=self.cleaned_data['end'],
            size=self.cleaned_data['required_size'])


class CarParkReservationForm(forms.Form):
    start = forms.DateTimeField(widget=DateTimeWidget, initial=initial_start)
    end = forms.DateTimeField(widget=DateTimeWidget, initial=initial_end)
    required_size = forms.IntegerField(min_value=1, initial=1)

    def __init__(self, car_park, *args, **kwargs):
        super(CarParkReservationForm, self).__init__(*args, **kwargs)
        self.fields['start'].widget = DateTimeWidget(options={ 'minuteStep': car_park.time_granulation })
        self.fields['end'].widget = DateTimeWidget(options={ 'minuteStep': car_park.time_granulation })
        self.car_park = car_park

    def reserve(self):
        if not self.is_valid():
            raise Exception('Form is not valid!')

        try:
            reservation = self.car_park.reserve(start=self.cleaned_data['start'], end=self.cleaned_data['end'],
                size=self.cleaned_data['required_size'], approved=False)
            
            if reservation:
                reservation.generate_token()
                reservation.generate_place_numbers()
                reservation.save()
                return reservation
            else:
                return None
        except SizeExceeded:
            self._errors['required_size'] = ['Reservation cannot be added. Size of the car park has been exceeded.']
            return None
        except ForbiddenHours:
            self._errors['__all__'] = ['Reservation period is outside park\'s working hours.']
            return None
        except TimeUnitNotDivisible:
            self._errors['__all__'] = ['Invalid reservation time. It must be divisible by {0} minutes.'.format(self.car_park.time_granulation)]
            return None
        except TooSoon:
            self._errors['__all__'] = ['Reservation start is too soon. It must be at least {0} minutes from now.'.format(self.car_park.not_sooner_than)]
            return None


class CarParkApprovalForm(forms.ModelForm):
    class Meta:
        model = CarParkReservation
        fields = ('firstname', 'lastname', 'email', 'phone_number',)

    def save(self, *args, **kwargs):
        reservation = super(CarParkApprovalForm, self).save(*args, **kwargs)
        if reservation and reservation.approve():
            return reservation
        else:
            return None
