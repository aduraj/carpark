from django.db import models, transaction
from django.contrib.auth.models import User
from kitabu.models.subjects import SubjectWithApprovableReservations, VariableSizeSubjectMixin, BaseSubject
from kitabu.models.reservations import ApprovableReservation, ReservationWithSize, BaseReservation
from registration.signals import user_registered
from django.dispatch import receiver
from django.core.exceptions import ValidationError
from kitabu.utils import now
import kitabu.models.validators
import binascii, os


class CarPark(SubjectWithApprovableReservations, VariableSizeSubjectMixin, BaseSubject):
    TIME_GRANULATION_CHOICES = (
        (5, '5 minutes'),
        (10, '10 minutes'),
        (15, '15 minutes'),
        (20, '20 minutes'),
        (30, '30 minutes'),
    )

    name = models.CharField(max_length=255)
    address = models.TextField(blank=True)
    open_at = models.TimeField(null=True, blank=True, help_text='Time format is HH:MM')
    close_at = models.TimeField(null=True, blank=True, help_text='Time format is HH:MM')
    time_granulation = models.PositiveIntegerField(default=5,
        help_text='Minimal unit (in minutes) for reservation start and end times. Defines time granulation.',
        choices=TIME_GRANULATION_CHOICES)
    not_sooner_than = models.PositiveIntegerField(default=60,
        help_text='Reservation cannot be added sooner than given minutes from now.')
    validity_minutes = models.PositiveIntegerField(default=3,
        help_text='For how long (in minutes) an unconfirmed reservation is active.')

    def working_hours_str(self):
        if self.open_at and self.close_at:
            return '{0} - {1}'.format(self.open_at, self.close_at)
        else:
            return '24/7'

    def clean(self, *args, **kwargs):
        if bool(self.open_at) != bool(self.close_at):
            raise ValidationError('You must provide both opening and closing time')

        if self.open_at and self.close_at and self.open_at >= self.close_at:
            raise ValidationError('Closing time must be greater than opening time')

        return super(CarPark, self).clean(*args, **kwargs)

    @classmethod
    def create_with_initial_data(cls):
        return cls.objects.create(name='Your car park', size=10, time_granulation=5, not_sooner_than=60,
            validity_minutes=3)

    def save(self, *args, **kwargs):
        with transaction.atomic():
            self.validity_period = "{0} * 60".format(self.validity_minutes)
            result = super(CarPark, self).save(*args, **kwargs)
            self.update_validators()
        return result

    def update_validators(self):
        try:
            time_interval_validator = TimeIntervalValidator.objects.filter(carpark__pk=self.pk)[0]
            time_interval_validator.time_value = self.not_sooner_than
            time_interval_validator.save()
        except IndexError:
            time_interval_validator = TimeIntervalValidator.objects.create(time_value=self.not_sooner_than,
                time_unit='minute', interval_type=TimeIntervalValidator.NOT_SOONER)
            self.validators.add(time_interval_validator)
        
        try:
            full_time_validator = FullTimeValidator.objects.filter(carpark__pk=self.pk)[0]
            full_time_validator.interval = self.time_granulation
            full_time_validator.save()
        except IndexError:
            full_time_validator = FullTimeValidator.objects.create(interval=self.time_granulation,
                interval_type='minute')
            self.validators.add(full_time_validator)

        try:
            opening_hours_validator = PeriodsInWeekdaysValidator.objects.filter(carpark__pk=self.pk)[0]
            if not self.open_at or not self.close_at:
                opening_hours_validator.delete()
            else:
                for period in opening_hours_validator.periods.all():
                    period.start = self.open_at
                    period.end = self.close_at
                    period.save()
        except IndexError:
            if self.open_at and self.close_at:
                opening_hours_validator = PeriodsInWeekdaysValidator.objects.create()
                for day_number in range(0, 7):
                    WithinDayPeriod.objects.create(weekday=day_number, start=self.open_at, end=self.close_at,
                        validator=opening_hours_validator)
                self.validators.add(opening_hours_validator)

class CarParkManager(models.Model):
    firstname = models.CharField(max_length=255, blank=True)
    lastname = models.CharField(max_length=255, blank=True)
    user = models.OneToOneField(User, related_name='car_park_manager')
    car_park = models.ForeignKey(CarPark, related_name='car_park_managers')

    @property
    def email(self):
        return self.user.email

    @property
    def username(self):
        return self.user.username

    @classmethod
    def create_with_initial_data(cls, user, car_park):
        return cls.objects.create(firstname='', lastname='', user=user, car_park=car_park)


class CarParkReservation(ApprovableReservation, ReservationWithSize, BaseReservation):
    subject = models.ForeignKey(CarPark, related_name='reservations')
    token = models.CharField(max_length=255, blank=True, unique=True, null=True)
    firstname = models.CharField(max_length=255, default='')
    lastname = models.CharField(max_length=255, default='')
    phone_number = models.CharField(max_length=255, default='')
    email = models.EmailField(max_length=255, default='')
    place_numbers_str = models.TextField(default='')

    def generate_token(self):
        new_token = self.get_new_token()
        while CarParkReservation.objects.filter(token=new_token).exists():
            new_token = self.get_new_token()
        self.token = new_token

    def get_new_token(self):
        return binascii.hexlify(os.urandom(15))

    def left_seconds(self):
        current_time = now()
        if self.valid_until <= current_time:
            return 0
        else:
            return (self.valid_until - current_time).seconds

    def place_numbers(self):
        if not self.place_numbers_str:
            return []
        else:
            return map(lambda s: int(s), self.place_numbers_str.split(','))

    def place_numbers_formatted(self):
        return ', '.join(map(lambda n: str(n), self.place_numbers()))

    def set_place_numbers(self, numbers):
        self.place_numbers_str = ','.join(map(lambda n: str(n), numbers))

    def generate_place_numbers(self):
        colliding_reservations = CarParkReservation.colliding_reservations(start=self.start, end=self.end,
            subject_id=self.subject_id).exclude(pk=self.pk).all()
        taken_places = sum(map(lambda r: r.place_numbers(), colliding_reservations), [])
        available_places = range(1, self.subject.size + 1)
        for p in taken_places:
            available_places.remove(p)
        self.set_place_numbers(available_places[0:self.size])


@receiver(user_registered)
def create_initial_data_for_user(sender, **kwargs):
    car_park = CarPark.create_with_initial_data()
    CarParkManager.create_with_initial_data(user=kwargs['user'], car_park=car_park)


# Validators

class FullTimeValidator(kitabu.models.validators.FullTimeValidator):
    pass


class TimeIntervalValidator(kitabu.models.validators.TimeIntervalValidator):
    pass


class PeriodsInWeekdaysValidator(kitabu.models.validators.PeriodsInWeekdaysValidator):
    pass


class WithinDayPeriod(kitabu.models.validators.WithinDayPeriod):
    validator = models.ForeignKey(PeriodsInWeekdaysValidator, related_name='periods')
