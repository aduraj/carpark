from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^search/', 'car_parks.views.search', name='car_parks_search'),
    url(r'^(?P<park_id>\d+)/reserve/', 'car_parks.views.reserve', name='car_parks_reserve'),
    url(r'^(?P<park_id>\d+)/confirm_reservation/(?P<reservation_token>[0-9a-zA-Z]+)/', 'car_parks.views.confirm_reservation',
        name='car_parks_confirm_reservation'),
    url(r'^(?P<park_id>\d+)/view_reservation/(?P<reservation_token>[0-9a-zA-Z]+)/', 'car_parks.views.view_reservation',
        name='car_parks_view_reservation'),
)
