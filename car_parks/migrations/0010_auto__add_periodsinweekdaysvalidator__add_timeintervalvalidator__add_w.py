# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PeriodsInWeekdaysValidator'
        db.create_table(u'car_parks_periodsinweekdaysvalidator', (
            (u'validator_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['kitabu.Validator'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'car_parks', ['PeriodsInWeekdaysValidator'])

        # Adding model 'TimeIntervalValidator'
        db.create_table(u'car_parks_timeintervalvalidator', (
            (u'validator_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['kitabu.Validator'], unique=True, primary_key=True)),
            ('time_value', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=1)),
            ('time_unit', self.gf('django.db.models.fields.CharField')(default='second', max_length=6)),
            ('interval_type', self.gf('django.db.models.fields.CharField')(default='s', max_length=2)),
            ('check_end', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'car_parks', ['TimeIntervalValidator'])

        # Adding model 'WithinDayPeriod'
        db.create_table(u'car_parks_withindayperiod', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('weekday', self.gf('django.db.models.fields.IntegerField')()),
            ('start', self.gf('django.db.models.fields.TimeField')()),
            ('end', self.gf('django.db.models.fields.TimeField')()),
            ('validator', self.gf('django.db.models.fields.related.ForeignKey')(related_name='periods', to=orm['car_parks.PeriodsInWeekdaysValidator'])),
        ))
        db.send_create_signal(u'car_parks', ['WithinDayPeriod'])

        # Adding model 'FullTimeValidator'
        db.create_table(u'car_parks_fulltimevalidator', (
            (u'validator_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['kitabu.Validator'], unique=True, primary_key=True)),
            ('interval', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('interval_type', self.gf('django.db.models.fields.CharField')(max_length=6)),
        ))
        db.send_create_signal(u'car_parks', ['FullTimeValidator'])

        # Deleting field 'CarPark.min_interval'
        db.delete_column(u'car_parks_carpark', 'min_interval')

        # Adding field 'CarPark.time_granulation'
        db.add_column(u'car_parks_carpark', 'time_granulation',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=5),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'PeriodsInWeekdaysValidator'
        db.delete_table(u'car_parks_periodsinweekdaysvalidator')

        # Deleting model 'TimeIntervalValidator'
        db.delete_table(u'car_parks_timeintervalvalidator')

        # Deleting model 'WithinDayPeriod'
        db.delete_table(u'car_parks_withindayperiod')

        # Deleting model 'FullTimeValidator'
        db.delete_table(u'car_parks_fulltimevalidator')

        # Adding field 'CarPark.min_interval'
        db.add_column(u'car_parks_carpark', 'min_interval',
                      self.gf('django.db.models.fields.PositiveIntegerField')(default=5),
                      keep_default=False)

        # Deleting field 'CarPark.time_granulation'
        db.delete_column(u'car_parks_carpark', 'time_granulation')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'car_parks.carpark': {
            'Meta': {'object_name': 'CarPark'},
            'address': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'close_at': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'not_sooner_than': ('django.db.models.fields.PositiveIntegerField', [], {'default': '60'}),
            'open_at': ('django.db.models.fields.TimeField', [], {'null': 'True', 'blank': 'True'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'time_granulation': ('django.db.models.fields.PositiveIntegerField', [], {'default': '5'}),
            'validators': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['kitabu.Validator']", 'symmetrical': 'False', 'blank': 'True'}),
            'validity_minutes': ('django.db.models.fields.PositiveIntegerField', [], {'default': '3'}),
            'validity_period': ('django.db.models.fields.CharField', [], {'default': "'120'", 'max_length': '13'})
        },
        u'car_parks.carparkmanager': {
            'Meta': {'object_name': 'CarParkManager'},
            'car_park': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'car_park_managers'", 'to': u"orm['car_parks.CarPark']"}),
            'firstname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'car_park_manager'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'car_parks.carparkreservation': {
            'Meta': {'object_name': 'CarParkReservation'},
            'approved': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'db_index': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'default': "''", 'max_length': '255'}),
            'end': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'firstname': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lastname': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'phone_number': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'size': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'start': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'reservations'", 'to': u"orm['car_parks.CarPark']"}),
            'token': ('django.db.models.fields.CharField', [], {'max_length': '255', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'valid_until': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'db_index': 'True'})
        },
        u'car_parks.fulltimevalidator': {
            'Meta': {'object_name': 'FullTimeValidator'},
            'interval': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'interval_type': ('django.db.models.fields.CharField', [], {'max_length': '6'}),
            u'validator_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['kitabu.Validator']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'car_parks.periodsinweekdaysvalidator': {
            'Meta': {'object_name': 'PeriodsInWeekdaysValidator'},
            u'validator_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['kitabu.Validator']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'car_parks.timeintervalvalidator': {
            'Meta': {'object_name': 'TimeIntervalValidator'},
            'check_end': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'interval_type': ('django.db.models.fields.CharField', [], {'default': "'s'", 'max_length': '2'}),
            'time_unit': ('django.db.models.fields.CharField', [], {'default': "'second'", 'max_length': '6'}),
            'time_value': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '1'}),
            u'validator_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['kitabu.Validator']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'car_parks.withindayperiod': {
            'Meta': {'object_name': 'WithinDayPeriod'},
            'end': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start': ('django.db.models.fields.TimeField', [], {}),
            'validator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'periods'", 'to': u"orm['car_parks.PeriodsInWeekdaysValidator']"}),
            'weekday': ('django.db.models.fields.IntegerField', [], {})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'kitabu.validator': {
            'Meta': {'object_name': 'Validator'},
            'actual_validator_related_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'apply_to_all': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        }
    }

    complete_apps = ['car_parks']