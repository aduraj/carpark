from django.shortcuts import render, redirect, get_object_or_404
from car_parks.models import CarPark
from car_parks.forms import CarParkSearchForm, CarParkReservationForm, CarParkApprovalForm
from django.contrib import messages
from kitabu.exceptions import OutdatedReservationError


def home(request):
    return render(request, 'car_parks/home.html', {
        'car_parks': CarPark.objects.all()
    })


def search(request):
    form = CarParkSearchForm(request.GET or None)
    if form.is_valid():
        car_parks = form.search()
    else:
        car_parks = CarPark.objects.all()

    return render(request, 'car_parks/search.html', {
        'car_parks': car_parks,
        'form': form
    })


def reserve(request, park_id):
    car_park = get_object_or_404(CarPark, pk=park_id)

    if request.POST:
        form = CarParkReservationForm(car_park=car_park, data=request.POST)
        if form.is_valid():
            reservation = form.reserve()
            if reservation:
                return redirect('car_parks_confirm_reservation', reservation_token=reservation.token,
                    park_id=car_park.id)
    else:
        form = CarParkReservationForm(car_park=car_park)
    
    return render(request, 'car_parks/reserve.html', {
        'car_park': car_park,
        'form': form
    })


def confirm_reservation(request, park_id, reservation_token):
    car_park = get_object_or_404(CarPark, pk=park_id)
    reservation = get_object_or_404(car_park.reservations, token=reservation_token, approved=False)

    if not reservation.is_valid():
        messages.error(request, 'This reservation is outdated and cannot be confirmed')
        return redirect('car_parks_reserve', park_id=car_park.id)

    if request.POST:
        form = CarParkApprovalForm(instance=reservation, data=request.POST)
        if form.is_valid():
            try:
                if form.save():
                    return redirect('car_parks_view_reservation', park_id=car_park.id, reservation_token=reservation.token)
                else:
                    messages.error(request, 'This reservation cannot be confirmed')
                    return redirect('car_parks_confirm_reservation', park_id=car_park.id, reservation_token=reservation.token)
            except OutdatedReservationError:
                messages.error(request, 'This reservation is outdated and cannot be confirmed')
                return redirect('car_parks_confirm_reservation', park_id=car_park.id, reservation_token=reservation.token)
    else:
        form = CarParkApprovalForm(instance=reservation)

    return render(request, 'car_parks/confirm_reservation.html', {
        'car_park': car_park,
        'reservation': reservation,
        'form': form,
    })


def view_reservation(request, park_id, reservation_token):
    car_park = get_object_or_404(CarPark, pk=park_id)
    reservation = get_object_or_404(car_park.reservations, token=reservation_token, approved=True)

    return render(request, 'car_parks/view_reservation.html', {
        'car_park': car_park,
        'reservation': reservation
    })
